#ifndef EventAction_h
#define EventAction_h 1

#include "G4UserEventAction.hh"

#include "G4THitsMap.hh"
#include "globals.hh"
#include <fstream>

class RunAction;
//Added
class G4LogicalVolume;
class DetectorConstruction;

class EventAction : public G4UserEventAction
{
public:
  EventAction(RunAction*, DetectorConstruction*);
  virtual ~EventAction();
  
  //virtual void  BeginOfEventAction(const G4Event*);
  //virtual void  EndOfEventAction(const G4Event*);
    
    virtual void  BeginOfEventAction(const G4Event* event);
    virtual void  EndOfEventAction(const G4Event* event);

  void AddEdep(G4double edep) { fEdepa0 += edep; }
  void AddStep(G4double step) { fStepa0 += step; }

  void AddEventsReaction0(G4int eventR) { fNReaction0 += eventR; }
      
  void Addedepstep(G4int i, G4double edep, G4double stepl) {
    fEdep[i] += edep;
    fStep[i] += stepl;
  }

private:
  //Added
  RunAction* fRunAction;
  //Added
  DetectorConstruction* fDetector;
  
  const G4int Divisions = 1000;
  G4int fNReaction0;
  G4double fEdepa0;
  G4double fStepa0;
  
  G4double fEdep[1000];
  G4double fStep[1000];
    
    // methods
    G4THitsMap<G4double>* GetHitsCollection(G4int hcID,
                                            const G4Event* event) const;
    G4double GetSum(G4THitsMap<G4double>* hitsMap) const;
    
    // data members
    G4int  fEfluxHCID;
    G4int  fEflux1HCID;
       
  // data members                   
  std::ofstream myFileAll;
};
                     
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif

    
