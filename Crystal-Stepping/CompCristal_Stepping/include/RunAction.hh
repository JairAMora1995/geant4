
#ifndef RunAction_h
#define RunAction_h 1

#include "G4UserRunAction.hh"

//Added
#include <fstream>
#include "globals.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class G4Run;
class HistoManager;

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

class RunAction : public G4UserRunAction
{

public:
  RunAction();
  ~RunAction();
  
  virtual void BeginOfRunAction(const G4Run*);
  virtual void EndOfRunAction(const G4Run*);
  
  //Added
  void AddEdepAllRun(G4double edep, G4double step){
    fedepAll += edep;
    fstepAll += step;
  }

  void TotalEventsR0(G4int R0) {fEventR0 += R0; }

  void nAddEdepAll(G4int i, G4double edep, G4double step){
    fEdepNAll[i] += edep;
    fStepNAll[i] += step;
  }


  G4int nofevents;
  
private:
  HistoManager* fHistoManager;
    
  const G4int Divisions2 = 1000;
  G4int fEventR0;

  G4double fEdepNAll[1000];
  G4double fStepNAll[1000];

  G4double fedepAll;
  G4double fstepAll;

  G4double let_all;


  
  // data members                   
  std::ofstream myFileNAll;
  
};

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

#endif



