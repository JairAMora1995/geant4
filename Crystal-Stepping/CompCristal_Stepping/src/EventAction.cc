#include "EventAction.hh"
//Added
#include "RunAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

#include "DetectorConstruction.hh"
#include "HistoManager.hh"
#include "Randomize.hh"
#include <iomanip>



//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
EventAction::EventAction(RunAction* run, DetectorConstruction* det)
  : G4UserEventAction(), fRunAction(run), fDetector(det), fEfluxHCID(-1),fEflux1HCID(-1)
{
  myFileAll.open("LET_Cristal_Event.dat", std::ios::out);
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
EventAction::~EventAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4THitsMap<G4double>*
EventAction::GetHitsCollection(G4int hcID,
                                  const G4Event* event) const
{
  auto hitsCollection
    = static_cast<G4THitsMap<G4double>*>(
        event->GetHCofThisEvent()->GetHC(hcID));
  
  if ( ! hitsCollection ) {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection ID " << hcID;
    G4Exception("EventAction::GetHitsCollection()",
      "MyCode0003", FatalException, msg);
  }

  return hitsCollection;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double EventAction::GetSum(G4THitsMap<G4double>* hitsMap) const
{
  G4double sumValue = 0.;
  for ( auto it : *hitsMap->GetMap() ) {
    // hitsMap->GetMap() returns the map of std::map<G4int, G4double*>
    sumValue += *(it.second);
  }
  return sumValue;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void EventAction::BeginOfEventAction(const G4Event*)
{
  fNReaction0 = 0;
  fEdepa0 = 0.0;
  fStepa0 = 0.0;

  //Revisar valor de la variable "Divisiones"... debe coincidir con la variable "N" del
  //SteppingAction.cc
  for(int i=0; i<Divisions; i++){
    fEdep[i] = 0.0;
    fStep[i] = 0.0;
  }
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void EventAction::EndOfEventAction(const G4Event* event)
{
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
    G4HCofThisEvent* HCE = event->GetHCofThisEvent();
    if(!HCE) return;
    
    // Get hist collections IDs
    if ( fEfluxHCID < 0 ) {
   fEfluxHCID = G4SDManager::GetSDMpointer()->GetCollectionID("CDetector1/Flux");
    }
      
      
    if ( fEflux1HCID < 0 ) {
        fEflux1HCID = G4SDManager::GetSDMpointer()->GetCollectionID("DecProton/FluxProton");
    }

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
// Get sum values from hits collections
//auto absoFlux = GetSum(GetHitsCollection(fEfluxHCID, event));
    
    //..............000000000000oooooo Protones entrantes....................

  //**********************************
  //****** COMENTAR ESTA lÍNEA *******
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //**********************************
        
   G4THitsMap<G4double>* evtMap =
   (G4THitsMap<G4double>*)(HCE->GetHC(fEfluxHCID));

   std::map<G4int,G4double*>::iterator itr;

           
         //G4double cpn = 0;
          for (itr = evtMap->GetMap()->begin(); itr != evtMap->GetMap()->end(); itr++)
         {
           G4int copyNb  = (itr->first);
           G4double hit = *(itr->second);

           // Fill histograms
           //************************************
           //****** COMENTAR ESTA lÍNEA *********
           analysisManager->FillH1(1, hit);
           //************************************
           
           if (myFileAll.is_open()) // Check if file is opened
             myFileAll << copyNb << "\t" <<  hit << G4endl;
           else
             G4cout << " !!!!!!! Error with myFile !!!!!!!! " << G4endl;
         }
    
//...........00000000000000oooooooooooo00000000oooo Protones salientes

    G4THitsMap<G4double>* evtMap4 =
     (G4THitsMap<G4double>*)(HCE->GetHC(fEflux1HCID));

   std::map<G4int,G4double*>::iterator itr4;

   
   //G4double cpn = 0;
    for (itr4 = evtMap4->GetMap()->begin(); itr4 != evtMap4->GetMap()->end(); itr4++)
   {
     G4int copyNb  = (itr4->first);
     G4double hit = *(itr4->second);

     // Fill histograms
     //************************************
     //****** COMENTAR ESTA lÍNEA *********
     analysisManager->FillH1(2, hit);
     //************************************
     
     if (myFileAll.is_open()) // Check if file is opened
       myFileAll << copyNb << "\t" <<  hit << G4endl;
     else
       G4cout << " !!!!!!! Error with myFile !!!!!!!! " << G4endl;
   }
         
         
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

    
    
  //Run* run = static_cast<Run*>(G4RunManager::GetRunManager()->GetNonConstCurrentRun());
  //G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();

  /*
  for(int i=0; i<Divisions; i++){
    if(fStep[i] <= 0.0) continue;
      myFileAll<< i + 0.5 << "   " <<fEdep[i]/keV<<"  "<<fStep[i]/um<<"  "<<fEdep[i]/fStep[i]/(keV/um)<<G4endl;
    }
  */
  fRunAction->TotalEventsR0(fNReaction0);
  fRunAction->AddEdepAllRun(fEdepa0, fStepa0);
   
  for(int i=0; i<Divisions; i++){
    fRunAction->nAddEdepAll(i, fEdep[i], fStep[i]);
  }
  
  //G4int NNN = fDetector->GetZPartition();
      
} 

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
