#include "RunAction.hh"
#include "HistoManager.hh"
#include "G4Run.hh"

//Added
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::RunAction()
  : G4UserRunAction(), fHistoManager(0)
{
fHistoManager = new HistoManager();
    
  const G4double letunit = 1.0*keV/um;
  const G4double stepunit = 1.0*um;
  const G4double edepunit = 1.0*keV;
  
  new G4UnitDefinition("keV/micra", "keV/micra" , "Let", letunit);
  new G4UnitDefinition("micras", "micras" , "step", stepunit);
  new G4UnitDefinition("keV", "keV" , "edep", edepunit);
  
  myFileNAll.open("LET_Cristal.dat", std::ios::out);
  
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

RunAction::~RunAction()
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::BeginOfRunAction(const G4Run*)
{
  fEventR0 = 0;
 
  fedepAll = 0.0;
  fstepAll = 0.0;
  
  for(int i=0; i<Divisions2; i++){
    fEdepNAll[i] = 0.0;
    fStepNAll[i] = 0.0;
  }
    
//.....................................................
    //histograms
    //
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    if ( analysisManager->IsActive() ) {
      analysisManager->OpenFile();
    }
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void RunAction::EndOfRunAction(const G4Run* run)
{
  nofevents =  run->GetNumberOfEvent();
  G4cout << "nofevents: " << nofevents << G4endl;
  
    G4double uDz =0.4069;
    G4double uDzMed =0.20345;
  for(int i=0; i<Divisions2; i++){
    if(fStepNAll[i] <= 0.0) continue;
    myFileNAll << i <<" "<< (i*uDz) + uDzMed << " " << fEdepNAll[i] << " " << fStepNAll[i] << " " << fEdepNAll[i]/fStepNAll[i] << G4endl;
      
      
      //****** COMENTAR ESTA lÍNEA *******
      G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
      //**********************************
      G4double PZmed = (i*uDz) + uDzMed;
      analysisManager->FillH1(3, fEdepNAll[i]/fStepNAll[i]);
      analysisManager->FillH1(4, fEdepNAll[i]);
      analysisManager->FillH1(5, PZmed);
      
      
      
      analysisManager->FillNtupleDColumn(0, 0, fEdepNAll[i]);
      analysisManager->FillNtupleDColumn(0, 1, PZmed);
      
      analysisManager->AddNtupleRow();
      
      G4cout << PZmed << "\t" << fEdepNAll[i] << "\t" << G4endl;
  }
   
   
    
  let_all = fedepAll/fstepAll;
   
    
  G4cout
    << G4endl
    << " The run consists of " << nofevents << " events. "
    << G4endl
    << " Cumulated LET per run, in scoring volume: "
    << G4BestUnit(let_all,"Let") 
    << G4endl
    << "------------------------------------------------------------"
    << G4endl
    << G4endl;
  //.........................................
    //save histograms
    //
    G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    if ( analysisManager->IsActive() ) {
      analysisManager->Write();
      analysisManager->CloseFile();
        
    }
    
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
