//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

   //Definición del material de las esferas
  G4Material* cobre = nist->FindOrBuildMaterial("G4_Cu");

  //Material Boro

  G4double z, a, fractionmass, density;
  G4String name, symbol;
  G4int ncomponents;
  a = 10.0*g/mole;
  G4Element* elB10 = new G4Element(name="Boro10",symbol="N" , z= 5., a);
  a = 11.0*g/mole;
  G4Element* elB11 = new G4Element(name="Boro11"
				 ,symbol="O" , z= 5., a);
  density = 2.37*g/cm3;
  G4Material* BoroM = new G4Material(name="BoroM  ",density,ncomponents=2);
  BoroM->AddElement(elB10, fractionmass=99*perCent);
  BoroM->AddElement(elB11, fractionmass=1*perCent);

  //Material Lutecio, Silicio y oxígeno
  G4double z_m, a_m, density_m;
  G4String nombre, simbolo;
  G4int n_components, natoms;
  a_m = 174.96*g/mole;
  G4Element* elLu
    = new G4Element(nombre="Lutecio", simbolo="Lu" , z_m= 71., a_m);
  a_m = 16.00*g/mole;
  G4Element* elO = new G4Element(nombre="Oxygen" , simbolo="O" , z_m= 8., a_m);
  a_m = 28.08*g/mole;
  G4Element* elSi
    = new G4Element(nombre="Silicio", simbolo="H" , z_m= 14., a_m);
 
  density_m = 1.000*g/cm3;
  G4Material* Lu2SiO5 = new G4Material(nombre="Lu_Si_O", density_m, n_components=3);
  Lu2SiO5->AddElement(elLu, natoms=2);
  Lu2SiO5->AddElement(elO, natoms=5);
  Lu2SiO5->AddElement(elSi, natoms=1);
 
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  G4double world_sizeXY = 5.0*meter;
  G4double world_sizeZ  = 5.0*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  
// Detector
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     //Esfera
  G4double Rmin = 0.0*meter;
  G4double Rmax = 1.0*meter;
  G4double SPhi = 0.0*degree;
  G4double DPhi = 360.0*degree;
  G4double STheta= 0.0*degree;
  G4double DTheta= 180.0*degree;
     
  G4Sphere* solidSphere =
    new G4Sphere("HSphere", Rmin, Rmax, SPhi, DPhi, STheta, DTheta); //its size
  
  //Caja
  G4double X= 0.5*meter;
  G4double Y = 1.0*meter;
  G4double SZ = 0.5*meter;

  G4Box* Box = new G4Box("SBox", X, Y, SZ); //its size

  //Resta
  G4SubtractionSolid* boxSphere =
    new G4SubtractionSolid("Box-Sphere", solidSphere, Box);


 //Logical volume
  G4LogicalVolume* logicSphere = 
    new G4LogicalVolume
            (boxSphere, //its solid
			cobre, //its material
			"LBox"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, 1*meter), //at (0,0,0)
		    logicSphere, //its logical volume
		    "PhysSphere", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

  //Caja con material Boro
  G4double sx= 0.5*meter;
  G4double sy = 0.8*meter;
  G4double sz = 0.5*meter;

  G4Box* BoxM = new G4Box("SBoxM", sx, sy, sz); //its size

 //Logical volume
  G4LogicalVolume* logicBoxM = 
    new G4LogicalVolume(BoxM, //its solid
			BoroM, //its material
			"LBoxM"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(), //at (0,0,0)
		    logicBoxM, //its logical volume
		    "PhysBoxM", //its name
		    logicSphere, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps


  //0o0o0o0o0o0o0o0o0oo00o0o0o0o0o0o0o0o0o00o0o0o0o0o00o0o0o0o0o0o000o
  //Esfera 2
   G4double rmin = 0.0*meter;
  G4double rmax = 1.0*meter;
  G4double sPhi = 0.0*degree;
  G4double dPhi = 360.0*degree;
  G4double sTheta= 0.0*degree;
  G4double dTheta= 180.0*degree;
     
  G4Sphere* solidSphere2 =
    new G4Sphere("HSphere2", rmin, rmax, sPhi, dPhi, sTheta, dTheta); //its size
   //Caja 2
  G4double sX= 0.5*meter;
  G4double sY = 1.0*meter;
  G4double sZ = 0.5*meter;

  G4Box* Box2 = new G4Box("SBox2", sX, sY, sZ); //its size

  //Resta
  G4SubtractionSolid* boxSphere2 =
    new G4SubtractionSolid("Box-Sphere2", solidSphere2, Box2);


 //Logical volume
  G4LogicalVolume* logicSphere2 = 
    new G4LogicalVolume(boxSphere2, //its solid
			cobre, //its material
			"LBox2"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, -1*meter), //at (0,0,0)
		    logicSphere2, //its logical volume
		    "PhysSphere2", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

  //Caja con material Lutecio, oxígeno y Lutecio
  G4double s_x= 0.5*meter;
  G4double s_y = 0.8*meter;
  G4double s_z = 0.5*meter;

  G4Box* BoxM2 = new G4Box("SBoxM2", s_x, s_y, s_z); //its size

 //Logical volume
  G4LogicalVolume* logicBoxM2 = 
    new G4LogicalVolume(BoxM2, //its solid
			Lu2SiO5, //its material
			"LBoxM2"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(), //at (0,0,0)
		    logicBoxM2, //its logical volume
		    "PhysBoxM2", //its name
		    logicSphere2, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps




  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

