//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4RotationMatrix.hh"

//sensitiveDetector
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh"
//#include "G4PSFlatSurfaceFlux.hh"
#include "G4PSTrackLength.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSDoseDeposit.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  

  //Definici�n del material del Target
  G4Material* agua = nist->FindOrBuildMaterial("G4_WATER");


  
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   G4double world_sizeXY = 15.0*meter;
  G4double world_sizeZ  = 47.0*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps  
 
  

 // Detector Caja de H2O
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  G4int ncelx = 40;
  G4int ncely = 40;
  G4int ncelz = 1;
  
  G4double Lx = 4.0*meter;
  G4double Ly = 4.0*meter;
  G4double Lz = 1.0*meter;
  G4double Dx = Lx/ncelx;
  G4double Dy = Ly/ncely;
  G4double Dz = Lz/ncelz;
 
  
  
  //Solid volume
  G4Box* solidBox = new G4Box("SBox",
				0.5*Lx,
				0.5*Ly,
				0.5*Lz); //its size
  
  //Logical volume
  G4LogicalVolume* logicBox =                         
    new G4LogicalVolume(solidBox,          //its solid
                        agua,         //its material
                        "LBox");            //its name
     
  
  for(G4int i = 0; i < ncelx; i++)
    {
      G4double D_x = (i)*Dx;
            
      for(G4int l = 0; l < ncely; l++)
	{
	  G4double D_y = (l)*Dy;

       	
	  for(G4int n = 0; n < ncelz; n++)
	    {
	      G4double D_z = (n)*Dz;

	     
	      G4ThreeVector u = G4ThreeVector(D_x-(1.5*meter),D_y-(1.0*meter),D_z+(15*meter));
	  G4ThreeVector position = u;
        
	  new G4PVPlacement(0,          //
			position,       // 
			logicBox,            //its logical volume
			"PhysBox", //its name
			logicWorld, //its mother volume
			false, //no boolean operation
			i, //copy number
			fCheckOverlaps); // checking overlaps
	    }
	}
    }
   


  
  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //auto absDetector = new G4MultiFunctionalDetector("RDetector");
  G4MultiFunctionalDetector* absDetector = new G4MultiFunctionalDetector("RDetector");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetector);

  G4PSFlatSurfaceCurrent* scorer = new G4PSFlatSurfaceCurrent("Flux", fCurrent_In);
  scorer->DivideByArea(false);
  absDetector->RegisterPrimitive(scorer);

   //Hacer Sencible el Detector
  SetSensitiveDetector("LBox",absDetector);

  G4PSTrackLength* scorer1 = new G4PSTrackLength("TrackL", fCurrent_In);
  scorer1->DivideByVelocity(false);
  absDetector->RegisterPrimitive(scorer1);

  G4PSEnergyDeposit* scorer2 = new G4PSEnergyDeposit("EneDep", fCurrent_In);
  absDetector->RegisterPrimitive(scorer2);

  G4PSDoseDeposit* scorer3 = new G4PSDoseDeposit("EneDosis",fCurrent_In);
  absDetector->RegisterPrimitive(scorer3);


  

 
    
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
