//Define el haz incidente

#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
//#include "G4ChargedGeantino.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "Randomize.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
  : G4VUserPrimaryGeneratorAction(),
    fParticleGun(0)
{
   G4int n_particle = 1;
    fParticleGun = new G4ParticleGun(n_particle);
  
  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* particle = particleTable->FindParticle("gamma");
  fParticleGun->SetParticleDefinition(particle);


  //Ubicación de la fuente de partículas incidentes (beam)
  fParticleGun->SetParticlePosition(G4ThreeVector(0.,0.,0.));
  //energía de la fuente de partículas incidentes (beam)
  fParticleGun->SetParticleEnergy(0.025*eV);
  //dirección de la fuente de partículas incidentes (beam)
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(4.0*micrometer,0.,1.));
  
}

//....oooOO0OOooo........oooOO0OOooo.......oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  /*
  //Fuente planar linear monoenergética
  G4double x1 = 0.0*micrometer, y1 = 0.0*micrometer;
  G4double Dx1 =  (2.0*G4UniformRand() - 1.0)*micrometer;
  G4double Dy1 =  (2.0*G4UniformRand() - 1.0)*micrometer;
		  x1 = x1 + Dx1;
		  y1 = y1 + Dy1;

		  */
	  
         

  G4double R=5.0*micrometer;
  G4int NBox= 4;
  G4int RandX = -4*G4UniformRand()+4;
  G4int RandY = -4*G4UniformRand()+4;
  G4double RandAX =  (-1.7*G4UniformRand()+0.7)*micrometer;
  G4double RandAY = (-2.0*G4UniformRand()+0.9)*micrometer;

  G4double RandXX = (-(NBox-1)+2*RandX)*R+RandAX;
  G4double RandYY = ((-(NBox-1)+2*RandY)*R+RandAY)+5.1*micrometer;

  G4ThreeVector *u =  new G4ThreeVector(RandXX, RandYY, -NBox*R-1.0*micrometer);

  fParticleGun->SetParticlePosition(*u);
  //fParticleGun->SetParticlePosition(G4ThreeVector(x1, y1, 0*micrometer));

		  
  
  
  //Create the vertex of the primary particle passing it to the G4Event pointer
  fParticleGun->GeneratePrimaryVertex(anEvent); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

