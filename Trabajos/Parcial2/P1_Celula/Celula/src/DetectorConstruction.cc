//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4RotationMatrix.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

   //Material del citoplasma
  G4Material* water = nist->FindOrBuildMaterial("G4_WATER");

  //Material del núcleo (Boro10)
  G4double z, a, fractionmass, density;
  G4String name, symbol;
  G4int ncomponents;
  a = 10.0*g/mole;
  G4Element* elB10 = new G4Element(name="Boro10",symbol="B" , z= 5., a);
  density = 2.37*g/cm3;
  G4Material* Boro_10 = new G4Material(name="Boro_10  ",density,ncomponents=1);
  Boro_10->AddElement(elB10, fractionmass=100*perCent);
  

 
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  G4int ncelx = 4;
  G4int ncely = 4;
  G4int ncelz = 4;
  G4double Lx = 40.0*micrometer;
  G4double Ly = 40.0*micrometer;
  G4double Lz = 40.0*micrometer;
  G4double Dx = Lx/ncelx;
  G4double Dy = Ly/ncely;
  G4double Dz = Lz/ncelz;


  G4double world_sizeXY = 200.0*micrometer;
  G4double world_sizeZ  = 200.0*micrometer;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  
// Detector
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
     //Citoplasma
  G4double Rmin = 0.0*micrometer;
  G4double Rmax = 5.0*micrometer;
  G4double SPhi = 0.0*degree;
  G4double DPhi = 360.0*degree;
  G4double STheta= 0.0*degree;
  G4double DTheta= 180.0*degree;
     
  G4Sphere* solidSphere =
    new G4Sphere("HSphere", Rmin, Rmax, SPhi, DPhi, STheta, DTheta); //its size
  
  //Hueco
  G4double X= 2.0*micrometer;
  G4double Y = 4.6*micrometer;
  G4double SZ = 2.0*micrometer;

  G4Box* Box = new G4Box("SBox", X/2, Y, SZ/2); //its size

  //Resta
  G4SubtractionSolid* boxSphere =
    new G4SubtractionSolid("Box-Sphere", solidSphere, Box);


 //Logical volume
  G4LogicalVolume* logicSphere = 
    new G4LogicalVolume(boxSphere, //its solid
			water, //its material
			"LBox"); //its name


  
  for(G4int i = 0; i < ncelx; i++)
    {
      G4double D_x = (i)*Dx;
            
      for(G4int l = 0; l < ncely; l++)
	{
	  G4double D_y = (l)*Dy;

       	
	  for(G4int n = 0; n < ncelz; n++)
	    {
	      G4double D_z = (n)*Dz;

	      G4RotationMatrix* rotm  = new G4RotationMatrix();
	      rotm->rotateX(90.0*deg);

	      G4ThreeVector u = G4ThreeVector(D_x-(15*micrometer),D_y-(10*micrometer),D_z+(20*micrometer));
	  G4ThreeVector position = u;
        
	  new G4PVPlacement(rotm,          //
			position,       // 
			logicSphere,            //its logical volume
			"PhysSphere", //its name
			logicWorld, //its mother volume
			false, //no boolean operation
			i, //copy number
			fCheckOverlaps); // checking overlaps
	    }
	}
    }


  //Núcleo de Boro 10
  G4double sx = 2.0*micrometer;
  G4double sy = 4.6*micrometer;
  G4double sz = 2.0*micrometer;

  G4Box* BoxM = new G4Box("SBoxM", sx/2, sy, sz/2); //its size

 //Logical volume
  G4LogicalVolume* logicBoxM = 
    new G4LogicalVolume(BoxM, //its solid
			Boro_10, //its material
			"LBoxM"); //its name

   //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(), //at (0,0,0)
		    logicBoxM, //its logical volume
		    "PhysBoxM", //its name
		    logicSphere, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps



  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

