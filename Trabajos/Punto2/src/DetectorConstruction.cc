//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

   //Definición del material de las esferas
  G4Material* Aluminio = nist->FindOrBuildMaterial("G4_Al");
  G4Material* Deuterio = nist->FindOrBuildMaterial("G4_Pb");
  G4Material* cobre = nist->FindOrBuildMaterial("G4_Cu");
  
 
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  G4double world_sizeXY = 10.0*meter;
  G4double world_sizeZ  = 10.0*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  
// Detector
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
  
  //Caja
  G4double X= 1.0*meter;
  G4double Y = 1.0*meter;
  G4double sZ = 2.0*meter;

  G4Box* Box = new G4Box("SBox", X, Y, sZ); //its size

   //Cilindro
  G4double RMin = 0.0*meter;
  G4double Rmax = 0.7*meter;
  G4double Z = 2.2*meter;
  G4double phi = 0.0*degree;
  G4double Dphi = 360.0*degree;
    
  //Solid volume
  G4Tubs* solidTube = new G4Tubs("STube", RMin, Rmax, Z, phi, Dphi); //its size

  //Resta
  G4SubtractionSolid* boxcyl =
    new G4SubtractionSolid("Box-cylinder", Box, solidTube);


 //Logical volume
  G4LogicalVolume* logicboxcyl = 
    new G4LogicalVolume(boxcyl, //its solid
			Deuterio, //its material
			"LBoxcyl"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, 0), //at (0,0,0)
		    logicboxcyl, //its logical volume
		    "Physboxcyl", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

  //Cilindro interno

   //Cilindro
  G4double RMin2 = 0.0*meter;
  G4double Rmax2 = 0.7*meter;
  G4double Z2 = 2.0*meter;
  G4double phi2 = 0.0*degree;
  G4double Dphi2 = 360.0*degree;
    
  //Solid volume
  G4Tubs* solidTube2 = new G4Tubs("STube2", RMin2, Rmax2, Z2, phi2, Dphi2); //its size

  // Logical volume
  G4LogicalVolume* logicTube2 = 
    new G4LogicalVolume(solidTube2, //its solid
			Aluminio, //its material
			"LTube2"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(), //at (0,0,0)
		    logicTube2, //its logical volume
		    "PhysTube2", //its name
		    logicboxcyl, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

    //Cilindro hueco1 o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0
  G4double RMin3 = 0.5*meter;
  G4double Rmax3 = 0.7*meter;
  G4double Z3 = 1.0*meter;
  G4double phi3 = 0.0*degree;
  G4double Dphi3 = 360.0*degree;
    
  //Solid volume
  G4Tubs* solidTube3 = new G4Tubs("STube3", RMin3, Rmax3, Z3, phi3, Dphi3); //its size

  // Logical volume
  G4LogicalVolume* logicTube3 = 
    new G4LogicalVolume(solidTube3, //its solid
			cobre, //its material
			"LTube3"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, 3.0*meter), //at (0,0,0)
		    logicTube3, //its logical volume
		    "PhysTube3", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

  //Cilindro hueco2 o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0
  G4double RMin4 = 0.5*meter;
  G4double Rmax4 = 0.7*meter;
  G4double Z4 = 1.0*meter;
  G4double phi4 = 0.0*degree;
  G4double Dphi4 = 360.0*degree;
    
  //Solid volume
  G4Tubs* solidTube4 = new G4Tubs("STube4", RMin4, Rmax4, Z4, phi4, Dphi4); //its size

  // Logical volume
  G4LogicalVolume* logicTube4 = 
    new G4LogicalVolume(solidTube4, //its solid
			cobre, //its material
			"LTube4"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, -3.0*meter), //at (0,0,0)
		    logicTube4, //its logical volume
		    "PhysTube4", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

