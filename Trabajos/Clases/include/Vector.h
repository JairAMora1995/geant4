//class definition ------------------------------
class Vector //Definición de la clase "Vector"
{ //Inicio de la clase "Vector"
public:
int x, y, z; //Declaración componentes del vector
public:
 Vector(int, int, int); //Constructor
 Vector(); //Otro constructor
~Vector(); //Destructor
double Length(); //Función para calcular magnitud del vector
}; //Fin de la clase "Vector"
//--------------------------------- 
