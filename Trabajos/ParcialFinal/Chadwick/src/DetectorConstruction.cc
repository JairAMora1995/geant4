
#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"
#include "G4ParticleDefinition.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4RotationMatrix.hh"
#include "G4SDParticleFilter.hh"

//Sensitive Detector
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh"
#include "G4PSEnergyDeposit.hh"

#include "G4PSMyScorer.hh"
#include "G4PSMyScorer2.hh"
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  
  
  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  

  //Definición de materiales
  G4Material* Plomo = nist->FindOrBuildMaterial("G4_Pb");
  G4Material* Parafina = nist->FindOrBuildMaterial("G4_PARAFFIN");
  G4Material* Berilio = nist->FindOrBuildMaterial("G4_Be");
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

   
  G4double world_sizeXY = 0.3*meter;
  G4double world_sizeZ  = 0.5*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  

  //=0=0o0o00o0oo0o0o0o0o0o0o0o0o0o0o0o0o0o0o0oooo0oo00o0oooooooo
  //Fuente Esfera
  G4double Rmin = 4.0*cm;
  G4double Rmax = 4.1*cm;
  G4double SPhi = 0.0*degree;
  G4double DPhi = 360.0*degree;
  G4double STheta= 0.0*degree;
  G4double DTheta= 176.0*degree;
     
  //Solid volume
  G4Sphere* solidSphere =
    new G4Sphere("HSphere", Rmin, Rmax, SPhi, DPhi, STheta, DTheta); //its size
  
  //Logical volume
  G4LogicalVolume* logicSphere  =                         
    new G4LogicalVolume(solidSphere,          //its solid
                        Plomo,         //its material
                        "LHSphere");            //its name

  G4RotationMatrix* rotm  = new G4RotationMatrix();
      rotm->rotateX(180.0*degree);
  
  //Physical volume
  new G4PVPlacement(rotm,                     //no rotation
		    G4ThreeVector(0., 0., -12*cm),       //at (0,0,0)
		    logicSphere,            //its logical volume
		    "PhysSphere",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 

  
  
 // Beryllium
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  G4double BoxB_x = 15.*cm;
  G4double BoxB_y = 15.*cm;
  G4double BoxB_z = 10.0*cm;
  G4Box* BoxBer = new G4Box("Box_Ber", BoxB_x/2, BoxB_y/2, BoxB_z/2);


   G4LogicalVolume* logicBoxBer = 
    new G4LogicalVolume(BoxBer, //its solid
			Berilio, //its material
			"B_Beryllium"); //its name 

   new G4PVPlacement(0,           //
		     G4ThreeVector(0.,0., 0.*cm),       // 
			logicBoxBer,            //its logical volume
			"B_Beryllium",               //its name
			logicWorld,                     //its mother  volume
			false,                 //no boolean operation
			0,                     //copy number
			fCheckOverlaps);       // checking overlaps

   
   //%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&%&

   //Paraffin
  
   G4double Par_x = 25.0*cm;
   G4double Par_y = 25.0*cm;
   G4double Par_z = 1.8*cm;
   G4Box* solidParaffin = new G4Box("BoxParaffin", Par_x/2, Par_y/2, Par_z/2);
   
   
   G4LogicalVolume* logicParaffin = 
     new G4LogicalVolume(solidParaffin, //its solid
			 Parafina, //its material
			 "Paraff"); //its name 
   
   new G4PVPlacement(0,           //
		     G4ThreeVector(0., 0., 8*cm),       // 
		     logicParaffin,            //its logical volume
		     "Paraff",               //its name
		     logicWorld,                     //its mother  volume
		     false,                 //no boolean operation
		     0,                     //copy number
		     fCheckOverlaps);       // checking overlaps

   

   
  //0o0o0o0o00o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o00oooo0

  //Targets
   
 //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   //Target1

   G4double Target1_x =15.*cm;
   G4double Target1_y =15.*cm;
   G4double Target1_z = 0.5*micrometer;
   G4Box* solidTarget1 = new G4Box("target1", Target1_x/2, Target1_y/2, Target1_z/2);
   
   
   G4LogicalVolume* logicTarget1 = 
     new G4LogicalVolume(solidTarget1, //its solid
			 Vacuum, //its material
			 "Target1"); //its name 
   
   new G4PVPlacement(0,           //
		     G4ThreeVector(0., 0., -5.5*cm),       // 
		     logicTarget1,            //its logical volume
		     "Target1",               //its name
		     logicWorld,                     //its mother  volume
		     false,                 //no boolean operation
		     0,                     //copy number
		     fCheckOverlaps);       // checking overlaps
   
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%% 
  
   //Target2
   
   G4double Target2_x =28.*cm;
   G4double Target2_y =28.*cm;
   G4double Target2_z = 0.5*micrometer;
   G4Box* solidTarget2 = new G4Box("target2", Target2_x/2, Target2_y/2, Target2_z/2);
   
   
   G4LogicalVolume* logicTarget2 = 
     new G4LogicalVolume(solidTarget2, //its solid
			 Vacuum, //its material
			 "Target2"); //its name 
   
   new G4PVPlacement(0,           //
		     G4ThreeVector(0., 0., 6.5*cm),       // 
		     logicTarget2,            //its logical volume
		     "Target2",               //its name
		     logicWorld,                     //its mother  volume
		     false,                 //no boolean operation
		     0,                     //copy number
		     fCheckOverlaps);       // checking overlaps
     
     
  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


void DetectorConstruction::ConstructSDandField()
{
G4SDManager::GetSDMpointer()->SetVerboseLevel(1);


//Flux Target1
 auto absDetector = new G4MultiFunctionalDetector("Absorber");
 G4SDManager::GetSDMpointer()->AddNewDetector(absDetector);
 G4PSFlatSurfaceCurrent* scorer = new G4PSFlatSurfaceCurrent("Flux",fCurrent_In);
 scorer->DivideByArea(false);
 absDetector->RegisterPrimitive(scorer);
 SetSensitiveDetector("Target1",absDetector);

 
//Flux Target2
 G4String filterName, particleName;
 G4SDParticleFilter* nFilter = new G4SDParticleFilter(filterName="nFilter",particleName="neutron"); 
 auto absDetector2 = new G4MultiFunctionalDetector("Absorber2");
 G4SDManager::GetSDMpointer()->AddNewDetector(absDetector2);
 G4PSFlatSurfaceCurrent* scorer2 = new G4PSFlatSurfaceCurrent("Flux2",fCurrent_In);
 scorer2->DivideByArea(false);
 scorer2->SetFilter(nFilter);
 absDetector2->RegisterPrimitive(scorer2);
 SetSensitiveDetector("Target2",absDetector2);

  /*G4String filterName, particleName;
   G4SDParticleFilter* nFilter = new G4SDParticleFilter(filterName="nFilter",particleName="neutron"); 
   auto absDetector2 = new G4MultiFunctionalDetector("Absorber2"); 
   G4SDManager::GetSDMpointer()->AddNewDetector(absDetector2); 
   //Flux.... that's the one!! 
   G4PSFlatSurfaceCurrent* scorer2 = new G4PSFlatSurfaceCurrent("Flux2",fCurrent_In); 
   scorer2->DivideByArea(false); 
   scorer2->SetFilter(nFilter); 
   absDetector2->RegisterPrimitive(scorer2); 
   //Make sensitive the detector 
   SetSensitiveDetector("LFlux2",absDetector2);*/ 
 
 //My scorer
 auto absDetector3 = new G4MultiFunctionalDetector("Absorber3");
 G4SDManager::GetSDMpointer()->AddNewDetector(absDetector3);
 G4PSMyScorer* scorer3 =  new G4PSMyScorer("Angular3");
 absDetector3->RegisterPrimitive(scorer3);
 SetSensitiveDetector("B_Beryllium",absDetector3);

 //My scorer2
 auto absDetector4 = new G4MultiFunctionalDetector("Absorber4");
 G4SDManager::GetSDMpointer()->AddNewDetector(absDetector4);
 G4PSMyScorer2* scorer4 =  new G4PSMyScorer2("Angular4");
 absDetector4->RegisterPrimitive(scorer4);
 SetSensitiveDetector("Paraff",absDetector4);


}

