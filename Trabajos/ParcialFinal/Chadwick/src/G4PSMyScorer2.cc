#include "G4PSMyScorer2.hh"
#include "G4UnitsTable.hh"

#include <string>
#include <fstream>

#include "HistoManager.hh"

 G4PSMyScorer2::G4PSMyScorer2(G4String name2, G4int depth2)
   :G4VPrimitiveScorer(name2,depth2),HCID2(-1) 
 {

   myFile.open("TestSalida3.dat", std::ios::out);
   }


G4PSMyScorer2::~G4PSMyScorer2()
{;}

G4bool G4PSMyScorer2::ProcessHits(G4Step* aStep2,G4TouchableHistory*)
{

  //Check for newly produced particle. e.g. first step.
  //if ( aStep->GetTrack()->GetCurrentStepNumber() == 0) return FALSE;

  //Check for this is not a primary particle. e.g ParentID > 0.
  //if ( aStep->GetTrack()->GetParentID() == 0) return FALSE;
  //if ( (aStep->GetTrack()->GetParentID() == 0) || (aStep->GetTrack()->GetParentID() > 1) ) return FALSE;


  //- Check the particle if the particle definition is given
  //if ( "neutron" && f != aStep->GetTrack()->GetDefinition() ) return FALSE;

  
  //G4double edep = aStep->GetTotalEnergyDeposit();
  //if ( edep == 0. ) return FALSE;
  
  G4double ekin2 = aStep2->GetPreStepPoint()->GetKineticEnergy();
  if ( ekin2 <= 0. ) return FALSE;

  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
    
  if (aStep2->GetTrack()->GetParentID() == 0) return FALSE;

  G4String PName2;
  PName2 = aStep2->GetTrack()->GetDefinition()->GetParticleName();

 

  if (PName2 == "proton"){

  //  std::cout << PName << std::endl;
    
    G4Track* track2 = aStep2->GetTrack();
    const G4DynamicParticle* dynParticle2 = track2->GetDynamicParticle();
    G4double kinEnergy2 = dynParticle2->GetKineticEnergy();
    G4double vertexkinEnergy2 = track2->GetVertexKineticEnergy();
    // const G4ThreeVector Direction2 = track->GetMomentumDirection();
    
 
    analysisManager->FillH1(7, vertexkinEnergy2);
    
    myFile << PName2 << "\t" << vertexkinEnergy2  << "\t" << kinEnergy2 << "\t" << G4endl;
    
     }
  
  // edep *= aStep->GetPreStepPoint()->GetWeight(); // (Particle Weight)
  //G4int  index = GetIndex(aStep);
  //EvtMap->add(index,edep);  
  return TRUE;
}

void G4PSMyScorer2::Initialize(G4HCofThisEvent* HCE2)
{
  EvtMap = new G4THitsMap<G4double>(GetMultiFunctionalDetector()->GetName(),
				    GetName());
  if(HCID2 < 0) {HCID2 = GetCollectionID(0);}
  HCE2->AddHitsCollection(HCID2, (G4VHitsCollection*)EvtMap);
}

void G4PSMyScorer2::EndOfEvent(G4HCofThisEvent*)
{;}

void G4PSMyScorer2::clear()
{
  EvtMap->clear();
}
