//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4RotationMatrix.hh"

//Sensitive Detector
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh" 
//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  

  //Definición del material del Target
  G4Material* Gold = nist->FindOrBuildMaterial("G4_Au");

  
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  G4double cryst_dX = 30*cm;
  G4double cryst_dY = 30*cm;
  G4double cryst_dZ = 5*cm;
  G4int nb_cryst = 72;
  G4double dphi = (360.0*degree)/nb_cryst;
  G4double dphi_half = dphi/2.0;
  G4double R1 = 0.5*cryst_dX/tan(dphi_half);
  G4double R2 = (R1 + cryst_dZ)/cos(dphi_half);

  G4cout << "R1: " << R1 << G4endl;
  G4cout << "R2: " << R2 << G4endl;
  
  G4double world_sizeXY = 2.0*R2 + 1.0*R1;
  G4double world_sizeZ  = 2.0*R2 + 1.0*R1;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  

 // Detectors
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
   
  //Solid volume
  G4Box* solidCryst = new G4Box("crystal",
			      cryst_dX/2,
			      cryst_dY/2,
			      cryst_dZ/2); //its size 
  //Logical volume
  G4LogicalVolume* logicCryst = 
    new G4LogicalVolume(solidCryst, //its solid
			Vacuum, //its material
			"Crystal"); //its name 


  
  for(G4int icrys = 2; icrys < nb_cryst-1; icrys++)
    {
      G4double phi = (icrys)*dphi;
      G4RotationMatrix* rotm  = new G4RotationMatrix();
      rotm->rotateY(phi);
      
      G4ThreeVector u = G4ThreeVector(std::cos(phi-90*degree),0,std::sin(phi-90*degree));
      G4ThreeVector position = (R1 + 0.5*cryst_dZ)*u;
      
      new G4PVPlacement(rotm,           //
			position,       // 
			logicCryst,            //its logical volume
			"crystal",               //its name
			logicWorld,                     //its mother  volume
			false,                 //no boolean operation
			icrys,                     //copy number
			fCheckOverlaps);       // checking overlaps
    }
  
  //0o0o0o0o00o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o0o00oooo0

  //Target
  
  G4double Target_x = 1*cm;
  G4double Target_y = 1*cm;
  G4double Target_z = 0.2*micrometer;
  G4Box* solidTarget = new G4Box("target", Target_x/2, Target_y/2, Target_z/2);


   G4LogicalVolume* logicTarget = 
    new G4LogicalVolume(solidTarget, //its solid
			Gold, //its material
			"Target"); //its name 

   new G4PVPlacement(0,           //
		     G4ThreeVector(),       // 
			logicTarget,            //its logical volume
			"Target",               //its name
			logicWorld,                     //its mother  volume
			false,                 //no boolean operation
			0,                     //copy number
			fCheckOverlaps);       // checking overlaps

  
   /*
   //Definir color del volumen
   G4Colour colour (1.,0.,0,);
   logicCryst->SetVisAttributes(colour);
   */

  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{
G4SDManager::GetSDMpointer()->SetVerboseLevel(1);

//auto absDetector = new G4MultiFunctionalDetector("RDetector");
G4MultiFunctionalDetector* absDetector = new G4MultiFunctionalDetector("Absorber");
G4SDManager::GetSDMpointer()->AddNewDetector(absDetector);

G4PSFlatSurfaceCurrent* scorer = new G4PSFlatSurfaceCurrent("Flux",fCurrent_Out);
scorer->DivideByArea(false);
absDetector->RegisterPrimitive(scorer);

//Make sensitive the detector
SetSensitiveDetector("Crystal",absDetector);

}


