# The set of languages for which implicit dependencies are needed:
set(CMAKE_DEPENDS_LANGUAGES
  "CXX"
  )
# The set of files for implicit dependencies of each language:
set(CMAKE_DEPENDS_CHECK_CXX
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/exgps.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/exgps.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/ActionInitialization.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/ActionInitialization.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/GeometryConstruction.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/GeometryConstruction.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/HistoManager.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/HistoManager.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/PhysicsList.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/PhysicsList.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/PrimaryGeneratorAction.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/PrimaryGeneratorAction.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/RunAction.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/RunAction.cc.o"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/src/TrackingAction.cc" "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps-build/CMakeFiles/exgps.dir/src/TrackingAction.cc.o"
  )
set(CMAKE_CXX_COMPILER_ID "GNU")

# Preprocessor definitions for this target.
set(CMAKE_TARGET_DEFINITIONS_CXX
  "G4INTY_USE_QT"
  "G4INTY_USE_XT"
  "G4LIB_BUILD_DLL"
  "G4UI_USE_QT"
  "G4UI_USE_TCSH"
  "G4VIS_USE_OPENGL"
  "G4VIS_USE_OPENGLQT"
  "G4VIS_USE_OPENGLX"
  "QT_CORE_LIB"
  "QT_GUI_LIB"
  "QT_NO_DEBUG"
  "QT_OPENGL_LIB"
  "QT_PRINTSUPPORT_LIB"
  "QT_WIDGETS_LIB"
  )

# The include file search paths:
set(CMAKE_CXX_TARGET_INCLUDE_PATH
  "/usr/local/Geant4/geant4.10.06.p01-install/include/Geant4"
  "/home/kanaro/Documentos/semestre_virtual/Geant4/exgps/include"
  "/usr/include/x86_64-linux-gnu/qt5"
  "/usr/include/x86_64-linux-gnu/qt5/QtGui"
  "/usr/include/x86_64-linux-gnu/qt5/QtCore"
  "/usr/lib/x86_64-linux-gnu/qt5/mkspecs/linux-g++"
  "/usr/include/x86_64-linux-gnu/qt5/QtWidgets"
  "/usr/include/x86_64-linux-gnu/qt5/QtPrintSupport"
  "/usr/include/x86_64-linux-gnu/qt5/QtOpenGL"
  )

# Targets to which this target links.
set(CMAKE_TARGET_LINKED_INFO_FILES
  )

# Fortran module output directory.
set(CMAKE_Fortran_TARGET_MODULE_DIR "")
