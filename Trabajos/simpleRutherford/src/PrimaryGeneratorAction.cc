//Define el haz incidente

#include "PrimaryGeneratorAction.hh"

#include "G4Event.hh"
#include "G4ParticleGun.hh"
#include "G4ParticleTable.hh"
#include "G4ParticleDefinition.hh"
//#include "G4ChargedGeantino.hh"
#include "G4SystemOfUnits.hh"
#include "G4PhysicalConstants.hh"
#include "Randomize.hh"


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::PrimaryGeneratorAction()
  :G4VUserPrimaryGeneratorAction(),
    fParticleGun(0)
{
  G4int n_particle = 1;
  fParticleGun = new G4ParticleGun(n_particle);
  
  // default particle kinematic
  G4ParticleTable* particleTable = G4ParticleTable::GetParticleTable();
  G4ParticleDefinition* particle = particleTable->FindParticle("alpha");
  fParticleGun->SetParticleDefinition(particle);

  //Ubicación de la fuente de partículas incidentes (beam)
  fParticleGun->SetParticlePosition(G4ThreeVector(0.,0.,-1.0*meter));
  //energía de la fuente de partículas incidentes (beam)
  fParticleGun->SetParticleEnergy(5.5*MeV);
  //dirección de la fuente de partículas incidentes (beam)
  fParticleGun->SetParticleMomentumDirection(G4ThreeVector(0.,0.,1.));
  
}

//....oooOO0OOooo........oooOO0OOooo.......oooOO0OOooo........oooOO0OOooo......

PrimaryGeneratorAction::~PrimaryGeneratorAction()
{
  delete fParticleGun;
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void PrimaryGeneratorAction::GeneratePrimaries(G4Event* anEvent)
{

  /*
  //Fuente puntual isotrópica monoenergética
  G4double phi = twopi*G4UniformRand();
  
  G4double costheta = 2.0*G4UniformRand() - 1.0;
  G4double theta = acos(costheta);

  G4ThreeVector u(std::sin(theta)*cos(phi), std::sin(theta)*sin(phi), std::cos(theta));

  fParticleGun->SetParticleMomentumDirection(u);
  */
		  /*
  //Fuente planar linear monoenergética
  G4double x = 0.0*meter, y = 0.0*meter;
  G4double Dx =  (2.0*G4UniformRand() - 1.0)*cm;
  G4double Dy =  (2.0*G4UniformRand() - 1.0)*cm;
		  x = x + Dx;
		  y = y + Dy;
	  
		  fParticleGun->SetParticlePosition(G4ThreeVector(x, y, -1.0*meter));
		  
  //Fuente puntual lineal polienergética
  G4double a = 3.0*MeV;
  G4double b = 7.0*MeV;
  G4double E = (b-a)*G4UniformRand() + a;
  fParticleGun->SetParticleEnergy(E);
							     
		  */							     
  
  //Create the vertex of the primary particle passing it to the G4Event pointer
  fParticleGun->GeneratePrimaryVertex(anEvent); 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

