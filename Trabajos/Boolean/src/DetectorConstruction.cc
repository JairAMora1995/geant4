//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  

  //Definición del material del tubo del haz
  G4Material* cobre = nist->FindOrBuildMaterial("G4_Cu");


  //Material Boro

  G4double z, a, fractionmass, density;
  G4String name, symbol;
  G4int ncomponents;
  a = 14.01*g/mole;
  G4Element* elB10 = new G4Element(name="Boro10",symbol="N" , z= 7., a);
  a = 16.00*g/mole;
  G4Element* elB11 = new G4Element(name="Boro11"
				 ,symbol="O" , z= 8., a);
  density = 1.290*mg/cm3;
  G4Material* Air = new G4Material(name="Air ",density,ncomponents=2);
  Air->AddElement(elN, fractionmass=70*perCent);
  Air->AddElement(elO, fractionmass=30*perCent);
  
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  G4double world_sizeXY = 5.0*meter;
  G4double world_sizeZ  = 5.0*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  
// Detector
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  //Caja
  G4double X= 3.0*meter;
  G4double Y = 3.0*meter;
  G4double SZ = 3.0*meter;

  G4Box* Box = new G4Box("SBox", X, Y, SZ); //its size

  //Cilindro

  G4double RMin = 0.0*meter;
  G4double Rmax = 2.0*meter;
  G4double Z = 3.5*meter;
  G4double phi = 360.0*degree;
  G4double Dphi = 0.0*degree;
    
  G4Tubs* solidTube = new G4Tubs("STube", RMin, Rmax, Z, Dphi,phi); //its size

  //Resta
  G4SubtractionSolid* boxtube =
    new G4SubtractionSolid("Box-Cylinder", Box, solidTube);


 //Logical volume
  G4LogicalVolume* logicBox = 
    new G4LogicalVolume(boxtube, //its solid
			cobre, //its material
			"LBox"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(), //at (0,0,0)
		    logicBox, //its logical volume
		    "PhysBox", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps



  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

