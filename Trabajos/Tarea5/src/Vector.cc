#include "../include/Vector.hh"

using namespace std;

Vector::Vector(double x, double y, double z){
  X=x; Y=y; Z=z;
}

Vector::Vector(double x1, double y1, double z1, double x2, double y2, double z2){
  X1 = x1; Y1 = y1; Z1 = z1;
  X2 = x2; Y2 = y2; Z2 = z2;
}

Vector::~Vector(){

}

double Vector::Magnitude(){
  return sqrt(pow(X, 2.0) + pow(Y, 2.0) + pow(Z, 2.0));
}

double Vector::Polar_angle(){
  if(Z > 0.0)
    return atan(sqrt(pow(X, 2.0) + pow(Y, 2.0))/Z);
  else if(Z == 0.0)
    return pi/2.0;
  else
    return pi + atan(sqrt(pow(X, 2.0) + pow(Y, 2.0))/Z);
}

double Vector::Azimuth_angle(){
  return atan2(Y, X);  
}

double Vector::Dot_Product(){
  return X1*X2 + Y1*Y2 + Z1*Z2;
}
