#ifndef _Main_Random
#define _Main_Random

#include <iostream>
#include <stdlib.h>
#include <math.h>
#include <time.h>

using namespace std;

class Random{
  double Top_number;
  double Buttom_number;

 public:
  Random (double Tn, double Bn);
  ~Random();
  double Random_Number();

};
#endif
