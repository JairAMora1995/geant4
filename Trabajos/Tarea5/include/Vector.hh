#ifndef _Main_Vector
#define _Main_Vector

#include <math.h>
#include <iostream>
#include <stdlib.h>
#include <stdio.h>

const double pi = 6.0*atan(1.0/sqrt(3.0));

using namespace std;

class Vector{
  
  double X, Y, Z;
  double X1, Y1, Z1;
  double X2, Y2, Z2;
  
 public:
  
  Vector (double x, double y, double z);
  Vector (double x1, double y1, double z1, double x2, double y2, double z2);
  ~Vector();
  
  double Magnitude();
  double Polar_angle();
  double Azimuth_angle();
  double Dot_Product();
};
#endif
