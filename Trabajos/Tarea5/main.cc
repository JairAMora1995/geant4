#ifndef _Main_
#define _Main_
#include <iostream>
#include "include/Vector.hh"
#include "include/Random.hh"

Random *number;
Vector *V_1, *V_2;

using namespace std;
int main(){
  double x1, x2, y1, y2, z1, z2;

  number = new Random(90.0, -60.0);
  x1 = number -> Random_Number();
  x2 = number -> Random_Number();
  y1 = number -> Random_Number();
  y2 = number -> Random_Number();
  z1 = number -> Random_Number();
  z2 = number -> Random_Number();

  V_1 = new Vector(x1, y1, z1);
  cout << "----------------Se tiene para el vector uno---------------------" << endl;
  cout << "V1              = (" << x1 << ")x + (" << y1 << ")y + (" << z1 <<")z" << endl;   
  cout << "Magnitud        = " << V_1 -> Magnitude() << endl;
  cout << "Angulo Polar    = " << V_1 -> Polar_angle() << endl;
  cout << "Angulo azimutal = " << V_1 -> Azimuth_angle() << endl;
  cout << endl;

  V_2 = new Vector(x1, y1, z1, x2, y2, z2);
  cout << "----------------Se tiene para el vector dos---------------------" << endl;
  cout << "V2              = (" << x2 << ")x + (" << y2 << ")y + (" << z2 <<")z" << endl;
  cout << "El producto punto V1.V2 = " << V_2 -> Dot_Product() << endl;
  cout << "----------------------------------------------------------------" << endl;
  delete number;
  delete V_1;
  delete V_2;
  return 0;
}
#endif
