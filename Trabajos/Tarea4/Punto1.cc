
//Programa para calcular la energía de enlace B

#include <iostream>
#include <math.h>

using namespace std;

int main() //Función principal
{
  int A, Z;
  double a1 = 15.67;
  double a2 = 17.23;
  double a3 = 0.75;
  double a4 = 93.2;
  double a5, B;

  cout<<"Please, enter the value of the atomic number Z: ";
  cin>>Z;
  cout<<"Now, please enter the value of the mass number A: ";
  cin>>A;
  
  if(A % 2 != 0) {
	a5 = 0;
  } else if((A % 2 == 0) && (Z % 2 == 0)){
      a5 = 12.0;
    } else if((A % 2 == 0) && (Z % 2 != 0)){
      a5 = -12.0;
      } 
  
  B = a1*A-a2*pow(A, 2./3.)-a3*(pow(Z, 2))/(pow(A,1./3.))-a4*(pow(A-2*Z, 2))/A+ (a5)/(pow(A, 1./2.));
 
  cout<< "The binding energy B is: "<<B<<endl;
      
  return 0;
}
