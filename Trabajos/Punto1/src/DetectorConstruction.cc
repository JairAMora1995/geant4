//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"

#include "G4Box.hh"
#include "G4Polycone.hh"
#include "G4Sphere.hh"
#include "G4SubtractionSolid.hh"
#include "G4Tubs.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

   //Definición de material cajas
  G4Material* piel = nist->FindOrBuildMaterial("G4_SKIN_ICRP");
  G4Material* cerebro = nist->FindOrBuildMaterial("G4_BRAIN_ICRP");
  G4Material* craneo = nist->FindOrBuildMaterial("G4_MUSCLE_SKELETAL_ICRP");
  G4Material* wolframio = nist->FindOrBuildMaterial("G4_W");
  G4Material* Plomo = nist->FindOrBuildMaterial("G4_Pb");

  
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  G4double world_sizeXY = 1.0*meter;
  G4double world_sizeZ  = 1.0*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  
// Detector
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
      
  //Caja1
  G4double X1= 30.0*cm;
  G4double Y1 = 30.0*cm;
  G4double Z1 = 3.0*cm;

  G4Box* Box1 = new G4Box("SBox", X1, Y1, Z1); //its size
  

 //Logical volume
  G4LogicalVolume* logicBox1 = 
    new G4LogicalVolume(Box1, //its solid
			cerebro, //its material
			"LBox1"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, 20.0*cm), //at (0,0,0)
		    logicBox1, //its logical volume
		    "PhysBox1", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

   //Caja2
  G4double X2= 30.0*cm;
  G4double Y2 = 30.0*cm;
  G4double Z2 = 1.0*cm;

  G4Box* Box2 = new G4Box("SBox2", X2, Y2, Z2); //its size
  

 //Logical volume
  G4LogicalVolume* logicBox2 = 
    new G4LogicalVolume(Box2, //its solid
			craneo, //its material
			"LBox2"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, 16.0*cm), //at (0,0,0)
		    logicBox2, //its logical volume
		    "PhysBox2", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps
   //Caja3
  G4double X3= 30.0*cm;
  G4double Y3 = 30.0*cm;
  G4double Z3 = 0.3*cm;

  G4Box* Box3 = new G4Box("SBox3", X3, Y3, Z3); //its size
  

 //Logical volume
  G4LogicalVolume* logicBox3 = 
    new G4LogicalVolume(Box3, //its solid
			piel, //its material
			"LBox3"); //its name

  //Physical volume 
  new G4PVPlacement(0, //no rotation
		    G4ThreeVector(0, 0, 14.7*cm), //at (0,0,0)
		    logicBox3, //its logical volume
		    "PhysBox3", //its name
		    logicWorld, //its mother volume
		    false, //no boolean operation
		    0, //copy number
		    fCheckOverlaps); // checking overlaps

 
  //Policono

   G4double phiStart = 0.0*degree;
  G4double phiTotal = 360.0*degree;
  G4double numZPlanes = 2;
  G4double zPlane[2] = {0.0*cm, 8.0*cm};
  G4double rInner[2] = {3.0*cm, 3.0*cm};
  G4double rOuter[2] = {3.0*cm, 10.0*cm};
  
  G4Polycone* solidPolycone = new G4Polycone("SPolycone", phiStart, phiTotal, numZPlanes, zPlane, rInner, rOuter);


  //Logical volume
  G4LogicalVolume* logicPolycone =                         
    new G4LogicalVolume(solidPolycone,          //its solid
                        Plomo,         //its material
                        "LPolycone");            //its name
  
  //Physical volume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, 3.4*cm),       //at (0,0,0)
		    logicPolycone,            //its logical volume
		    "PhysPolycone",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 
  
  //Cilindro

   //Cilindro
  G4double RMin = 0.0*cm;
  G4double Rmax = 2.0*cm;
  G4double Z = 1.0*cm;
  G4double phi = 0.0*degree;
  G4double Dphi = 360.0*degree;
    
  //Solid volume
  G4Tubs* solidTube = new G4Tubs("STube", RMin, Rmax, Z, phi, Dphi); //its size
  
  //Logical volume
  G4LogicalVolume* logicTube =                         
    new G4LogicalVolume(solidTube,          //its solid
                        wolframio,         //its material
                        "LTube");            //its name
  
  //Physical volume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0,0, -2.5*cm),       //at (0,0,0)
		    logicTube,            //its logical volume
		    "PhysTube",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 
  
  



  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

