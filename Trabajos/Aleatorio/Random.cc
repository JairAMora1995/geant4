#include <stdlib.h>
#include <iostream>
#include <time.h>
#include <math.h>

using namespace std;

int main()
{
  int N = 10000;
  
  double x, y, r;
  int Ncir = 0;
  int Ncua = 0;
  double Acir = 0;
  double Acua = 4.0;

  srand(time(NULL));

  for(int i = 0; i < N; i++)
    {
      //r1 = rand() % 10;
      //r1 = (double) rand() / RAND_MAX;
      x = 2.0*((double) rand() / RAND_MAX)-1.0;
      y = 2.0*((double) rand() / RAND_MAX)-1.0;

      r= sqrt(x*x + y*y);

      if(r < 1.0)
	Ncir++;
      else
	Ncua++;
         }
  
    Acir = (1.0* Ncir / (Ncua + Ncir)) * Acua;

      cout << Acir <<endl;
     
  return 0;
}
