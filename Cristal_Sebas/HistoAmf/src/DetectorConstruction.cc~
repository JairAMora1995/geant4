//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

//sensitiveDetector
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh"
#include "G4SDParticleFilter.hh"
#include "G4PSTrackLength.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSDoseDeposit.hh"
#include "G4PSNofSecondary.hh"




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* man = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  


  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  G4double world_sizeXY = 30.0*mm;
  G4double world_sizeZ  = 30.0*mm;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  

  
  //0000000000000oooooooooooo Material Cristal oooooooooooo0000000
  //Material del Cristal (NaI con impurezas de Tl)
  
     
  G4double z, a, fractionmass, density;
  G4String name, symbol;
  G4int ncomponents;
  
  G4Material* NaI = man->FindOrBuildMaterial("G4_SODIUM_IODIDE");

  a=204.383*g/mole; 
  G4Element* Tl = new G4Element (name="Talio", symbol="Tl", z=80 , a);

  density = 3.67*g/cm3;
  G4Material* NaI_Tl = new G4Material(name="NaI(Tl) ",density,ncomponents=2);
  NaI_Tl->AddMaterial(NaI, fractionmass=99.99*perCent);
  NaI_Tl->AddElement(Tl, fractionmass=0.01*perCent);
   
   
   //imprima Material

    G4cout << *(G4Material::GetMaterialTable()) << G4endl;

  //--------------------------Cristal_Amorfo-------------

  G4double size_x = 2.*mm;
  G4double size_y = 12.*mm;
  G4double size_z = 2.*mm;

  G4Box* Amfsolid = new G4Box("Amf.NaI",
			      0.5*size_x,
			      0.5*size_y,
			      0.5*size_z);
  
  G4LogicalVolume* Amflogic =                         
    new G4LogicalVolume(Amfsolid,          //its solid
                        NaI_Tl,         //its material
                        "AmfLog");            //its name
  

 
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(),       //at (0,0,0)
		    Amflogic,            //its logical volume
		    "AmfPhys",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       //

  

  
   
 
  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{

  ////////////////////////////////////Detectores para el amorfo////////////////////////
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //auto absDetector = new G4MultiFunctionalDetector("RDetector");
  G4MultiFunctionalDetector* CrisDetector = new G4MultiFunctionalDetector("AmfDetector");
  G4SDManager::GetSDMpointer()->AddNewDetector(CrisDetector);
  //////////////////////1erContador///////////////////////////////////////////////////////////
  G4PSFlatSurfaceCurrent* scorer = new G4PSFlatSurfaceCurrent("Flux", fCurrent_In);
  scorer->DivideByArea(false);
  CrisDetector->RegisterPrimitive(scorer);



  
  G4PSEnergyDeposit* scorer2 = new G4PSEnergyDeposit("EneDep");
  CrisDetector->RegisterPrimitive(scorer2);

  G4PSDoseDeposit* scorer3 = new G4PSDoseDeposit("EneDosis");
  CrisDetector->RegisterPrimitive(scorer3);

  G4PSTrackLength* scorer1 = new G4PSTrackLength("TrackL");
  scorer1->DivideByVelocity(false);
  CrisDetector->RegisterPrimitive(scorer1);

  //Hacer Sencible el Detector
  SetSensitiveDetector("AmfLog",CrisDetector);






  
 
  }









  
