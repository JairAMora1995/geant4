//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

////////////////Librerias añadidas///////////////////////////////////
#include "G4Material.hh"
#include "G4CrystalExtension.hh"
#include "G4ExtendedMaterial.hh"
#include "G4LogicalCrystalVolume.hh"


//sensitiveDetector
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh"
#include "G4SDParticleFilter.hh"
#include "G4PSTrackLength.hh"
#include "G4PSEnergyDeposit.hh"
#include "G4PSDoseDeposit.hh"
#include "G4PSNofSecondary.hh"




//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* man = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* Vacuum   = new G4Material("Vacuum",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  


  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  G4double world_sizeXY = 100*cm;
  G4double world_sizeZ  = 200*cm;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        Vacuum,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps 
  
      
//CRISTAL
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
  
  //VolumenSolidoCristal.......................................
  G4Box* Cristal1 = new G4Box ("Cristal.NaI",
			       5.08*cm,
			       10.16*cm,
			       40.64*cm);

  
  //0000000000000oooooooooooo Material Cristal oooooooooooo0000000
  //Material del Cristal (NaI con impurezas de Tl)
  
     
  G4double z, a, fractionmass, density;
  G4String name, symbol;
  G4int ncomponents;
  
  G4Material* NaI = man->FindOrBuildMaterial("G4_SODIUM_IODIDE");

  a=204.383*g/mole; 
  G4Element* Tl = new G4Element (name="Talio", symbol="Tl", z=80 , a);

  density = 3.67*g/cm3;
  G4Material* NaI_Tl = new G4Material(name="NaI(Tl) ",density,ncomponents=2);
  NaI_Tl->AddMaterial(NaI, fractionmass=99.99*perCent);
  NaI_Tl->AddElement(Tl, fractionmass=0.01*perCent);

  G4ExtendedMaterial* Crystal =
      new G4ExtendedMaterial("crystal.material", NaI_Tl);
   
  
   //imprima Material

    G4cout << *(G4Material::GetMaterialTable()) << G4endl;

    //ContruccionCristal0000000000000000000000000000000OOOOOOOOooooooooooooooooooooo

    Crystal->RegisterExtension(std::unique_ptr<G4CrystalExtension>(
            new G4CrystalExtension(Crystal)));
   
    G4CrystalExtension* crystalExtension = (G4CrystalExtension*)Crystal->RetrieveExtension("crystal");
    
    crystalExtension->SetUnitCell(
				  new G4CrystalUnitCell(6.47 * CLHEP::angstrom,
							6.47 * CLHEP::angstrom,
							6.47 * CLHEP::angstrom,
							CLHEP::halfpi,
							CLHEP::halfpi,
							CLHEP::halfpi,
							225));
  
       
    //VolumenLogicoCristal.........................
    G4LogicalVolume* crystalLogic =
      new G4LogicalCrystalVolume(Cristal1,
                                 Crystal,
                                 "crystal_logic");
    
    //VolumenFisicoCristal.........................
         new G4PVPlacement(0,
                      G4ThreeVector(),
                      crystalLogic,
                      "crystal.physical",
                      logicWorld,
                      false,
			   0);
 

//MESH de vacio %%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
                G4int n_Mesh = 100;
                G4double Mesh_x = 5.080001*cm;
                G4double Mesh_y = 10.160001*cm;
                G4double Mesh_z = 40.64*cm;
                G4double Mesh_dz = (Mesh_z/n_Mesh);

        
        
            //Solid Volume
            G4Box* solidMesh = new G4Box("SMesh",
                                         Mesh_x,
                                         Mesh_y,
                                         Mesh_dz); //Its size of mesh
        
            //Logical Volume
            G4LogicalVolume* logicMesh =
                new G4LogicalVolume(solidMesh, //its solid volume
                                    Vacuum, //Its the material
                                    "LMesh"); //Its the name
        
            for(G4int iMesh = 0; iMesh < n_Mesh; iMesh++)
            {
                G4double Mesh_Dz = (iMesh)*Mesh_dz;
                
                G4ThreeVector u =
                G4ThreeVector (0, 0, (Mesh_Dz*(2))+(Mesh_dz-(40.64*cm)));
            
                G4ThreeVector Posicion = u;
            
                new G4PVPlacement(0,           //
                                  Posicion,       //
                                  logicMesh,            //its logical volume
                                  "PhyMesh",               //its name
                                  logicWorld,                     //its mother  volume
                                  false,                 //no boolean operation
                                  iMesh,                     //copy number
                                  fCheckOverlaps);       // checking overlaps
                   
            }
    
    //Placa contabilizadora de Protones Salientes%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%
    
    G4double box_sizeX = 7.09*cm;
    G4double box_sizeY = 14.17*cm;
    G4double box_sizeZ = 0.00001*cm;
    
    //Solid volume
    G4Box* solidBox = new G4Box("SBox",
                  box_sizeX,
                  box_sizeY,
                  box_sizeZ); //its size
    
    //Logical volume
    G4LogicalVolume* logicBox =
      new G4LogicalVolume(solidBox,          //its solid
                          Vacuum,         //its material
                          "LBox");            //its name
    
    //Physical volume
    new G4PVPlacement(0,                     //no rotation
              G4ThreeVector(0, 0, 49.0*cm),       //at (0,0,0)
              logicBox,            //its logical volume
              "PhysBox",               //its name
              logicWorld,                     //its mother  volume
              false,                 //no boolean operation
              0,                     //copy number
              fCheckOverlaps);       // checking overlaps
    
    

    
  //retorna objeto apuntador a la clase G4VPhysicalVolume
    return physWorld;
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{

  ////////////////////////////////////Detectores para el Cristal///////////////////////
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //auto absDetector = new G4MultiFunctionalDetector("RDetector");
  G4MultiFunctionalDetector* CrisDetector = new G4MultiFunctionalDetector("CDetector");
  G4SDManager::GetSDMpointer()->AddNewDetector(CrisDetector);
    
  //////////////////////1erContador///////////////////////////////////////////////////////////
  G4PSFlatSurfaceCurrent* scorer = new G4PSFlatSurfaceCurrent("Flux", fCurrent_In);
  scorer->DivideByArea(false);
  CrisDetector->RegisterPrimitive(scorer);
  
  G4PSEnergyDeposit* scorer2 = new G4PSEnergyDeposit("EneDep");
  CrisDetector->RegisterPrimitive(scorer2);

  G4PSDoseDeposit* scorer3 = new G4PSDoseDeposit("EneDosis");
  CrisDetector->RegisterPrimitive(scorer3);

  G4PSTrackLength* scorer1 = new G4PSTrackLength("TrackL");
  scorer1->DivideByVelocity(false);
  CrisDetector->RegisterPrimitive(scorer1);

  //Hacer Sencible el Detector
  SetSensitiveDetector("crystal_logic",CrisDetector);
    
    
    
    
    ///////////////////////2do Contador de Protones///////////////////////////////
    G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
    G4MultiFunctionalDetector* DetecProton = new G4MultiFunctionalDetector("DecProton");
    G4SDManager::GetSDMpointer()->AddNewDetector(DetecProton);
    
    G4PSFlatSurfaceCurrent* Flujo = new G4PSFlatSurfaceCurrent("FluxProton", fCurrent_In);
    Flujo->DivideByArea(false);
    DetecProton->RegisterPrimitive(Flujo);
    
    G4String filterName1, particleName1;
    G4SDParticleFilter* protonFilter = new G4SDParticleFilter(filterName1="proFilter", particleName1="proton");
    
    
    
    //Hacer Sencible el detector
    SetSensitiveDetector("LBox", DetecProton);


 
  }









  
