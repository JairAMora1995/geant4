//
// ********************************************************************
// * License and Disclaimer                                           *
// *                                                                  *
// * The  Geant4 software  is  copyright of the Copyright Holders  of *
// * the Geant4 Collaboration.  It is provided  under  the terms  and *
// * conditions of the Geant4 Software License,  included in the file *
// * LICENSE and available at  http://cern.ch/geant4/license .  These *
// * include a list of copyright holders.                             *
// *                                                                  *
// * Neither the authors of this software system, nor their employing *
// * institutes,nor the agencies providing financial support for this *
// * work  make  any representation or  warranty, express or implied, *
// * regarding  this  software system or assume any liability for its *
// * use.  Please see the license in the file  LICENSE  and URL above *
// * for the full disclaimer and the limitation of liability.         *
// *                                                                  *
// * This  code  implementation is the result of  the  scientific and *
// * technical work of the GEANT4 collaboration.                      *
// * By using,  copying,  modifying or  distributing the software (or *
// * any work based  on the software)  you  agree  to acknowledge its *
// * use  in  resulting  scientific  publications,  and indicate your *
// * acceptance of all terms of the Geant4 Software license.          *
// ********************************************************************
//
//
/// \file EventAction.cc
/// \brief Implementation of the B4dEventAction class

#include "EventAction.hh"

#include "G4RunManager.hh"
#include "G4Event.hh"
#include "G4SDManager.hh"
#include "G4HCofThisEvent.hh"
#include "G4UnitsTable.hh"
#include "G4SystemOfUnits.hh"

//**********************************
//****** COMENTAR ESTA lÍNEA *******
#include "HistoManager.hh"
//**********************************

#include "Randomize.hh"
#include <iomanip>

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
EventAction::EventAction()
  //Poner lo que aparece en B1EventAction Relacionado con el B1RunAction
 : G4UserEventAction(),
   fEfluxHCID(-1), fEflux1HCID(-1), fEflux2HCID(-1)
{
  myFile.open("WSalida.dat", std::ios::out);
}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
EventAction::~EventAction()
{}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4THitsMap<G4double>* 
EventAction::GetHitsCollection(G4int hcID,
                                  const G4Event* event) const
{
  auto hitsCollection 
    = static_cast<G4THitsMap<G4double>*>(
        event->GetHCofThisEvent()->GetHC(hcID));
  
  if ( ! hitsCollection ) {
    G4ExceptionDescription msg;
    msg << "Cannot access hitsCollection ID " << hcID; 
    G4Exception("EventAction::GetHitsCollection()",
      "MyCode0003", FatalException, msg);
  }         

  return hitsCollection;
}    


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
G4double EventAction::GetSum(G4THitsMap<G4double>* hitsMap) const
{
  G4double sumValue = 0.;
  for ( auto it : *hitsMap->GetMap() ) {
    // hitsMap->GetMap() returns the map of std::map<G4int, G4double*>
    sumValue += *(it.second);
  }
  return sumValue;  
}  


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void EventAction::BeginOfEventAction(const G4Event* /*event*/)
{}


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
void EventAction::EndOfEventAction(const G4Event* event)
{
  G4HCofThisEvent* HCE = event->GetHCofThisEvent();
  if(!HCE) return;
  
  // Get hist collections IDs
  if ( fEfluxHCID < 0 ) {
    fEfluxHCID = G4SDManager::GetSDMpointer()->GetCollectionID("RDetector/Flux");
  }
  if ( fEflux1HCID < 0 ) {
    fEflux1HCID = G4SDManager::GetSDMpointer()->GetCollectionID("RDetectorA/Flux1");
  }
   if ( fEflux2HCID < 0 ) {
    fEflux2HCID = G4SDManager::GetSDMpointer()->GetCollectionID("RDetectorAB/Flux2");
  }
  

  
  // Get sum values from hits collections
  //auto absoFlux = GetSum(GetHitsCollection(fEfluxHCID, event));


  
  G4THitsMap<G4double>* evtMap = 
    (G4THitsMap<G4double>*)(HCE->GetHC(fEfluxHCID));

  std::map<G4int,G4double*>::iterator itr;

  //**********************************
  //****** COMENTAR ESTA lÍNEA *******
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  //**********************************
  
  //G4double cpn = 0;
   for (itr = evtMap->GetMap()->begin(); itr != evtMap->GetMap()->end(); itr++)
  {
    G4int copyNb  = (itr->first);
    G4double hit = *(itr->second);

    // Fill histograms
    //************************************
    //****** COMENTAR ESTA lÍNEA *********
    analysisManager->FillH1(1, hit);
    //************************************
    
    if (myFile.is_open()) // Check if file is opened
      myFile << copyNb*4.5 << "\t" <<  hit << G4endl;
    else
      G4cout << " !!!!!!! Error with myFile !!!!!!!! " << G4endl;
  }

   /////////////////////////////////segudno contador//////////////////////////////////////

  
  G4THitsMap<G4double>* evtMap1 =
    (G4THitsMap<G4double>*)(HCE->GetHC(fEflux1HCID));

  std::map<G4int,G4double*>::iterator itr1;

  //**********************************
  //****** COMENTAR ESTA lÍNEA *******
  G4AnalysisManager* analysisManager1 = G4AnalysisManager::Instance();
  //**********************************
  
  //G4double cpn = 0;
   for (itr1 = evtMap1->GetMap()->begin(); itr1 != evtMap1->GetMap()->end(); itr1++)
  {
    G4int copyNb1  = (itr1->first);
    G4double hit1 = *(itr1->second);

    // Fill histograms
    //************************************
    //****** COMENTAR ESTA lÍNEA *********
    analysisManager1->FillH1(2, hit1);
    //************************************
    
    if (myFile.is_open()) // Check if file is opened
      myFile << copyNb1*4.5 << "\t" <<  hit1 << G4endl;
    else
      G4cout << " !!!!!!! Error with myFile !!!!!!!! " << G4endl;
  }


   /////////////////////////////////Tercer contador//////////////////////////////////////

  
   G4THitsMap<G4double>* evtMap2 =
     (G4THitsMap<G4double>*)(HCE->GetHC(fEflux2HCID));

   std::map<G4int,G4double*>::iterator itr2;

   //**********************************
   //****** COMENTAR ESTA lÍNEA *******
   G4AnalysisManager* analysisManager2 = G4AnalysisManager::Instance();
   //**********************************
   
   //G4double cpn = 0;
   for (itr2 = evtMap2->GetMap()->begin(); itr2 != evtMap2->GetMap()->end(); itr2++)
     {
       G4int copyNb2  = (itr2->first);
       G4double hit2 = *(itr2->second);
       
       // Fill histograms
       //************************************
       //****** COMENTAR ESTA lÍNEA *********
       analysisManager2->FillH1(7, hit2);
       //************************************
       
       if (myFile.is_open()) // Check if file is opened
	 myFile << copyNb2*4.5 << "\t" <<  hit2 << G4endl;
       else
	 G4cout << " !!!!!!! Error with myFile !!!!!!!! " << G4endl;
     }
   
   
   
   
}

   


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
