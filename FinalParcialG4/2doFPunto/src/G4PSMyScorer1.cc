#include "G4PSMyScorer1.hh"
#include "G4UnitsTable.hh"

#include <string>
#include <fstream>

#include "HistoManager.hh"

G4PSMyScorer1::G4PSMyScorer1(G4String name, G4int depth)
  :G4VPrimitiveScorer(name,depth),HCID(-1) 
{
  //SetUnit("MeV");
   myFile.open("TestSalida1.dat", std::ios::out);
}


G4PSMyScorer1::~G4PSMyScorer1()
 {;}


G4bool G4PSMyScorer1::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{
  //- check for newly produced particle. e.g. first step.
  if ( aStep->GetTrack()->GetCurrentStepNumber() == 0) return FALSE;
  
  //- check for this is not a primary particle. e.g. ParentID > 0 .
  //if ( aStep->GetTrack()->GetParentID() == 0 ) return FALSE;
  if ( (aStep->GetTrack()->GetParentID() == 0) || (aStep->GetTrack()->GetParentID() > 1) ) return FALSE;

  //- check the particle if the partifle definition is given.
  //if ( particleDef && particleDef != aStep->GetTrack()->GetDefinition() ) return FALSE;


  //G4double edep = aStep->GetTotalEnergyDeposit();
  //if ( edep == 0. ) return FALSE;
  G4double ekin2 = aStep->GetPreStepPoint()->GetKineticEnergy();
  if ( ekin2 <= 0. ) return FALSE;

  G4String PName; 
  PName = aStep->GetTrack()->GetDefinition()->GetParticleName();
  
  G4Track* track2 = aStep->GetTrack();
  const G4DynamicParticle* dynParticle2 = track2->GetDynamicParticle();  
  G4double kinEnergy2 = dynParticle2->GetKineticEnergy();
  //G4double vertexkinEnergy = track->GetVertexKineticEnergy();
  //const G4ThreeVector Direction = track->GetMomentumDirection();

  //G4double SLength = aStep->GetStepLength();
  
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->FillH1(4, kinEnergy2);

  myFile << PName << "\t" << ekin2 << "\t" << kinEnergy2 << "\t" << G4endl;
  
  //edep *= aStep->GetPreStepPoint()->GetWeight(); // (Particle Weight)
  //G4int  index = GetIndex(aStep);
  //EvtMap->add(index,edep);  
  return TRUE;
}


void G4PSMyScorer1::Initialize(G4HCofThisEvent* HCE)
{
   EvtMap = new G4THitsMap<G4double>(GetMultiFunctionalDetector()->GetName(),
                                     GetName());
   if(HCID < 0) {HCID = GetCollectionID(0);}
   HCE->AddHitsCollection(HCID, (G4VHitsCollection*)EvtMap);
}


void G4PSMyScorer1::EndOfEvent(G4HCofThisEvent*)
 {;}


void G4PSMyScorer1::clear() {
  EvtMap->clear();
}


