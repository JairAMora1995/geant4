#include "G4PSMyScorer2.hh"
#include "G4UnitsTable.hh"

#include <string>
#include <fstream>

#include "HistoManager.hh"

G4PSMyScorer2::G4PSMyScorer2(G4String name, G4int depth)
  :G4VPrimitiveScorer(name,depth),HCID(-1) 
{
  //SetUnit("MeV");
   myFile.open("TestSalida2.dat", std::ios::out);
}


G4PSMyScorer2::~G4PSMyScorer2()
 {;}


G4bool G4PSMyScorer2::ProcessHits(G4Step* aStep,G4TouchableHistory*)
{
  //- check for newly produced particle. e.g. first step.
  //if ( aStep->GetTrack()->GetCurrentStepNumber() == 0) return FALSE;
  
  //- check for this is not a primary particle. e.g. ParentID > 0 .
  //if ( aStep->GetTrack()->GetParentID() == 0 ) return FALSE;
  //if ( (aStep->GetTrack()->GetParentID() == 0) || (aStep->GetTrack()->GetParentID() > 1) ) return FALSE;

  //- check the particle if the partifle definition is given.
  // if ( particleDef && particleDef != aStep->GetTrack()->GetDefinition() ) return FALSE;


  //G4double edep = aStep->GetTotalEnergyDeposit();
  //if ( edep == 0. ) return FALSE;
  G4double ekin = aStep->GetPreStepPoint()->GetKineticEnergy();
  if ( ekin <= 0. ) return FALSE;

  G4String PName; 
  PName = aStep->GetTrack()->GetDefinition()->GetParticleName();
  
  G4Track* track = aStep->GetTrack();
  const G4DynamicParticle* dynParticle = track->GetDynamicParticle();  
  G4double kinEnergy = dynParticle->GetKineticEnergy();
  G4double vertexkinEnergy = track->GetVertexKineticEnergy();
  const G4ThreeVector Direction = track->GetMomentumDirection();

  //G4double SLength = aStep->GetStepLength();
  
  G4AnalysisManager* analysisManager = G4AnalysisManager::Instance();
  analysisManager->FillH1(5, Direction.theta()*180/CLHEP::pi);
  analysisManager->FillH1(6, Direction.phi()*180/CLHEP::pi);
  
  myFile << PName << vertexkinEnergy << "\t" << Direction.theta()*180/CLHEP::pi << "\t" << Direction.phi()*180/CLHEP::pi << "\t" << G4endl;
  
  //edep *= aStep->GetPreStepPoint()->GetWeight(); // (Particle Weight)
  //G4int  index = GetIndex(aStep);
  //EvtMap->add(index,edep);  
  return TRUE;
}


void G4PSMyScorer2::Initialize(G4HCofThisEvent* HCE)
{
   EvtMap = new G4THitsMap<G4double>(GetMultiFunctionalDetector()->GetName(),
                                     GetName());
   if(HCID < 0) {HCID = GetCollectionID(0);}
   HCE->AddHitsCollection(HCID, (G4VHitsCollection*)EvtMap);
}


void G4PSMyScorer2::EndOfEvent(G4HCofThisEvent*)
 {;}


void G4PSMyScorer2::clear() {
  EvtMap->clear();
}


