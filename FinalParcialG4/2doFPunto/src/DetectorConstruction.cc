//Construcción del target, detector

#include "DetectorConstruction.hh"

#include "G4NistManager.hh"
#include "G4Box.hh"
#include "G4Sphere.hh"
#include "G4Tubs.hh"
//Sustraccion
#include "G4SubtractionSolid.hh"

#include "G4LogicalVolume.hh"
#include "G4PVPlacement.hh"
#include "G4PhysicalConstants.hh"
#include "G4SystemOfUnits.hh"

#include "G4VisAttributes.hh"
#include "G4Colour.hh"

#include "G4RotationMatrix.hh"

//sensitiveDetector
#include "G4SDManager.hh"
#include "G4MultiFunctionalDetector.hh"
#include "G4VPrimitiveScorer.hh"
#include "G4PSFlatSurfaceCurrent.hh"

////MyScorers
#include "G4PSMyScorer.hh"
#include "G4PSMyScorer1.hh"
#include "G4PSMyScorer2.hh"
//#include "G4PSTrackLength.hh"
//#include "G4PSEnergyDeposit.hh"
//#include "G4PSDoseDeposit.hh"
//#include "G4PSNofSecondary.hh"
#include "G4SDParticleFilter.hh"

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::DetectorConstruction()
  : G4VUserDetectorConstruction(),
    fCheckOverlaps(true)
{}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

DetectorConstruction::~DetectorConstruction()
{ }


//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......


G4VPhysicalVolume* DetectorConstruction::Construct()
{
  // Llamado a la base de datos del NIST
  G4NistManager* nist = G4NistManager::Instance();

  //Definición del "vacío"
  G4double presion, temperatura, densidad;
  densidad     = universe_mean_density;    //from PhysicalConstants.h
  presion    = 3.e-18*pascal;
  temperatura = 2.73*kelvin;
  G4Material* vacio   = new G4Material("vacio",
                                        1., 1.01*g/mole, densidad,
                                        kStateGas,temperatura,presion);

  

  //Definici�n del material del Target
  G4Material* parafina = nist->FindOrBuildMaterial("G4_PARAFFIN");
  G4Material* berilio = nist->FindOrBuildMaterial("G4_Be");
  G4Material* plomo = nist->FindOrBuildMaterial("G4_Pb");
  


  
  // World ()
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  G4double world_sizeXY = 0.5*meter;
  G4double world_sizeZ  = 1.5*meter;
  
  //Solid volume
  G4Box* solidWorld = new G4Box("SolWorld",
				0.5*world_sizeXY,
				0.5*world_sizeXY,
				0.5*world_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicWorld =                         
    new G4LogicalVolume(solidWorld,          //its solid
                        vacio,         //its material
                        "LogWorld");            //its name
  
  //Physical volume
  G4VPhysicalVolume* physWorld = 
    new G4PVPlacement(0,                     //no rotation
                      G4ThreeVector(),       //at (0,0,0)
                      logicWorld,            //its logical volume
                      "PhysWorld",               //its name
                      0,                     //its mother  volume
                      false,                 //no boolean operation
                      0,                     //copy number
                      fCheckOverlaps);       // checking overlaps  
 
  

 // Detector 
  //%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%%

  /////////////////////////////esfera con cabidad Radiante//////////////////////

  G4double rMin = 0.094*meter ;
  G4double rMax = 0.098*meter;
  G4double sPhi = 0*degree;
  G4double dPhi = 360*degree;
  G4double sTheta = 0*degree;
  G4double dTheta = 360*degree;
  //VSolidoEsfera
  G4Sphere* Esfe1 = new G4Sphere ("SEsfe1", rMin, rMax, sPhi, dPhi, sTheta, dTheta);

  G4double RMin = 0;
  G4double RMax = 0.008*meter;
  G4double DZ = 0.098*meter;
  G4double RSPhi = 0*degree;
  G4double RDPhi = 360*degree;

  //VSolidoCilindro
  G4Tubs* Cilin1 = new G4Tubs ("SCilin1", RMin, RMax, DZ, RSPhi, RDPhi);

  G4SubtractionSolid* colimador = new  G4SubtractionSolid("Coli", Esfe1, Cilin1);


   //Logical volume
  G4LogicalVolume* logicColi =                         
    new G4LogicalVolume(colimador,          //its solid
                        plomo,         //its material
                        "LColi");            //its name

  
  //PhysicalVolume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, -0.6*meter),       //at (0,0,0)
		    logicColi,            //its logical volume
		    "PhyColi",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 
  
  
  /////////////////////////////////Contador 1 de alphas/////////////////////////////////////
  G4double box_sizeX = 0.398*meter;
  G4double box_sizeY = 0.398*meter;
  G4double box_sizeZ = 0.000003*meter;
  
  //Solid volume
  G4Box* solidBox = new G4Box("SBox",
				0.5*box_sizeX,
				0.5*box_sizeY,
				0.5*box_sizeZ); //its size
  
  //Logical volume
  G4LogicalVolume* logicBox =                         
    new G4LogicalVolume(solidBox,          //its solid
                        vacio,         //its material
                        "LBox");            //its name

  
  //PhysicalVolume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, -0.48*meter),       //at (0,0,0)
		    logicBox,            //its logical volume
		    "PhysBox",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 
 


  /////////////////////////////////////////Placa de Berilio//////////////////////////////////////////////

  G4double box_sizeX1 = 0.10*meter;
  G4double box_sizeY1 = 0.10*meter;
  G4double box_sizeZ1 = 0.14*meter;
  
  //Solid volume
  G4Box* solidBox1 = new G4Box("SBox1",
				0.5*box_sizeX1,
				0.5*box_sizeY1,
				0.5*box_sizeZ1); //its size
  
  //Logical volume
  G4LogicalVolume* logicBox1 =                         
    new G4LogicalVolume(solidBox1,          //its solid
                        berilio,         //its material
                        "LBox1");            //its name

  
  //PhysicalVolume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, -0.36*meter),       //at (0,0,0)
		    logicBox1,            //its logical volume
		    "PhysBox1",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps


  ////////////////////////////////////////////segunda caja de Vacio (Contador)////////////////////////////////////

  G4double box_sizeX2 = 0.48*meter;
  G4double box_sizeY2 = 0.48*meter;
  G4double box_sizeZ2 = 0.000005*meter;
  
  //Solid volume
  G4Box* solidBox2 = new G4Box("SBox2",
				0.5*box_sizeX2,
				0.5*box_sizeY2,
				0.5*box_sizeZ2); //its size
  
  //Logical volume
  G4LogicalVolume* logicBox2 =                         
    new G4LogicalVolume(solidBox2,          //its solid
                        vacio,         //its material
                        "LBox2");            //its name

  
  //PhysicalVolume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, 0.12*meter),       //at (0,0,0)
		    logicBox2,            //its logical volume
		    "PhysBox2",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 
 


  /////////////////////////////////////////Placa de Parafina//////////////////////////////////////////////

  G4double box_sizeX3 = 0.48*meter;
  G4double box_sizeY3 = 0.48*meter;
  G4double box_sizeZ3 = 0.05*meter;
  
  //Solid volume
  G4Box* solidBox3 = new G4Box("SBox3",
				0.5*box_sizeX3,
				0.5*box_sizeY3,
				0.5*box_sizeZ3); //its size
  
  //Logical volume
  G4LogicalVolume* logicBox3 =                         
    new G4LogicalVolume(solidBox3,          //its solid
                        parafina,         //its material
                        "LBox3");            //its name

  
  //PhysicalVolume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, 0.146*meter),       //at (0,0,0)
		    logicBox3,            //its logical volume
		    "PhysBox3",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps

  
 ////////////////////////////////////////////Ultima caja de Vacio (Contador)////////////////////////////////////

  G4double box_sizeX4 = 0.48*meter;
  G4double box_sizeY4 = 0.48*meter;
  G4double box_sizeZ4 = 0.000005*meter;
  
  //Solid volume
  G4Box* solidBox4 = new G4Box("SBox4",
				0.5*box_sizeX4,
				0.5*box_sizeY4,
				0.5*box_sizeZ4); //its size
  
  //Logical volume
  G4LogicalVolume* logicBox4 =                         
    new G4LogicalVolume(solidBox4,          //its solid
                        vacio,         //its material
                        "LBox4");            //its name

  
  //PhysicalVolume
  new G4PVPlacement(0,                     //no rotation
		    G4ThreeVector(0, 0, 0.2*meter),       //at (0,0,0)
		    logicBox4,            //its logical volume
		    "PhysBox4",               //its name
		    logicWorld,                     //its mother  volume
		    false,                 //no boolean operation
		    0,                     //copy number
		    fCheckOverlaps);       // checking overlaps 
 
  
  

  
  //retorna objeto apuntador a la clase G4VPhysicalVolume
  return physWorld; 
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......

void DetectorConstruction::ConstructSDandField()
{
  ////////////////////////////////////Detectores de vacio flujo de particulas////////////////////////
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //auto absDetector = new G4MultiFunctionalDetector("RDetector");
  G4MultiFunctionalDetector* absDetector = new G4MultiFunctionalDetector("RDetector");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetector);
  //////////////////////1erContador///////////////////////////////////////////////////////////
  G4PSFlatSurfaceCurrent* scorer = new G4PSFlatSurfaceCurrent("Flux", fCurrent_In);
  scorer->DivideByArea(false);
  absDetector->RegisterPrimitive(scorer);
  SetSensitiveDetector("LBox",absDetector);
  
  ////////////////////////2doContador/////////////////////////////////////////////////////////
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //auto absDetector = new G4MultiFunctionalDetector("RDetector");
  G4String filterName, particleName;
  G4SDParticleFilter* neutronFilter = new G4SDParticleFilter(filterName="neutronFilter", particleName="neutron");
  G4MultiFunctionalDetector* absDetectorA = new G4MultiFunctionalDetector("RDetectorA");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetectorA);
  
  G4PSFlatSurfaceCurrent* scorer1 = new G4PSFlatSurfaceCurrent("Flux1", fCurrent_Out);
  scorer1->DivideByArea(false);
  absDetectorA->RegisterPrimitive(scorer1);
  SetSensitiveDetector("LBox2",absDetectorA);

  //////////////////////////////////////////////MyScorers////////////////////////////////////
  /////////////////////Energia1erDetector/////////////////////////////

  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  G4MultiFunctionalDetector* absDetector1 = new G4MultiFunctionalDetector("RDetector1");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetector1);

  G4PSMyScorer* scorerKinEn1 = new G4PSMyScorer("kinEnergy");
  absDetector1->RegisterPrimitive(scorerKinEn1);
  SetSensitiveDetector("LBox",absDetector1);


   /////////////////////Energia2doDetector/////////////////////////////

  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  G4MultiFunctionalDetector* absDetector2 = new G4MultiFunctionalDetector("RDetector2");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetector2);

  G4PSMyScorer1* scorerKinEn2 = new G4PSMyScorer1("kinEnergy2");
  absDetector2->RegisterPrimitive(scorerKinEn2);
  SetSensitiveDetector("LBox3",absDetector2);

  /////////////////////DispercionAngular///////////////////////////////////
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  G4MultiFunctionalDetector* absDetector3 = new G4MultiFunctionalDetector("RDetector3");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetector3);

  G4PSMyScorer2* scorerAngu = new G4PSMyScorer2("Dis.Angular");
  absDetector3->RegisterPrimitive(scorerAngu);
  SetSensitiveDetector("LBox1",absDetector3);

  /////////////////////////////////////////////////////////////////////////Flujo Protones//////////////////////////////////
  G4SDManager::GetSDMpointer()->SetVerboseLevel(1);
  //auto absDetector = new G4MultiFunctionalDetector("RDetector");
  G4String filterName1, particleName1;
  G4SDParticleFilter* protonFilter = new G4SDParticleFilter(filterName1="protronFilter", particleName1="proton");
  G4MultiFunctionalDetector* absDetectorAB = new G4MultiFunctionalDetector("RDetectorAB");
  G4SDManager::GetSDMpointer()->AddNewDetector(absDetectorAB);
  
  G4PSFlatSurfaceCurrent* scorer1A = new G4PSFlatSurfaceCurrent("Flux2", fCurrent_In);
  scorer1A->DivideByArea(false);
  absDetectorAB->RegisterPrimitive(scorer1A);
  SetSensitiveDetector("LBox4",absDetectorAB);
  

  
  
  
   
}

//....oooOO0OOooo........oooOO0OOooo........oooOO0OOooo........oooOO0OOooo......
