#ifndef G4PSMyScorer2_h
#define G4PSMyScorer2_h 1

#include "G4VPrimitiveScorer.hh"
#include "G4THitsMap.hh"

class G4PSMyScorer2 : public G4VPrimitiveScorer
{
  
public: // with description
  G4PSMyScorer2(G4String name, G4int depth=0); // default unit
 
  virtual ~G4PSMyScorer2();
  
protected: // with description
  virtual G4bool ProcessHits(G4Step*,G4TouchableHistory*);
  
public: 
  virtual void Initialize(G4HCofThisEvent*);
  virtual void EndOfEvent(G4HCofThisEvent*);
  virtual void clear();

  std::ofstream myFile;
  
private:
  G4int HCID;
  G4THitsMap<G4double>* EvtMap;
  
  
};
#endif
